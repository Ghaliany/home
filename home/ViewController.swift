//
//  ViewController.swift
//  home
//
//  Created by mohammadali ghaliany on 1.06.2020.
//  Copyright © 2020 mohammadali ghaliany. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    /// This is for portrait 
    override public var shouldAutorotate: Bool {
      return false
    }
    override public var supportedInterfaceOrientations: UIInterfaceOrientationMask {
      return .portrait
    }
    override public var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
      return .portrait
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }


}

